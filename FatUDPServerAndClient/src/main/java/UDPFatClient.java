import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class UDPFatClient {

    private static final int TO_PORT = 1234;
    private static final String TO_MACHINE = "localhost";

    private static int BUFFER_SIZE = 40;

    public static void main(String[] args) {

        try {

            System.out.println("CREANDO EL SOCKET DE DATAGRAMAS");
            // Enlaza el socket con cualquier puerto disponible elegido  por el kernel
            DatagramSocket datagramSocket = new DatagramSocket();

            System.out.println(".... ENVIANDO SECUENCIA INICIO ...");
            String message = ">START<";

            DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(StandardCharsets.UTF_8),
                    message.getBytes(StandardCharsets.UTF_8).length,
                    InetAddress.getByName(TO_MACHINE),TO_PORT);

            datagramSocket.send(datagramPacket);
            System.out.println(".... SECUENCIA INICIO.");

            byte[] ebytes = GenerateMessageHelper.GenerarSecuencia('Ü',80000)
                    .getBytes(StandardCharsets.UTF_8);

            for (int i = 0; i< ebytes.length;i+=BUFFER_SIZE)
            {
                datagramPacket.setData(ebytes,i,BUFFER_SIZE);
                datagramSocket.send(datagramPacket);
            }

            byte[] bye_buffer = ">BYE<".getBytes(StandardCharsets.UTF_8);

            datagramPacket.setData(bye_buffer);

            datagramSocket.send(datagramPacket);

            System.out.println("CERRANDO EL SOCKET");
            datagramSocket.close();
            System.out.println("CLIENTE FINALIZADO");

        } catch (SocketException e){
            System.out.println("nERROR EN EL SOCKET: " + e.getLocalizedMessage());
        } catch (IOException e){
            System.err.println("nI/O ERROR: " + e.getLocalizedMessage());
        }

    }

}
