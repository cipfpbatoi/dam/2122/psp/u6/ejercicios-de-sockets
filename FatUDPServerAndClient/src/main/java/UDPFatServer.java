import java.io.IOException;
import java.net.*;

public class UDPFatServer {

    private static final int PORT = 1234;
    private static final String MACHINE = "localhost";

    public static void main(String[] args) {

        try {
            System.out.println("CREANDO SOCKET DE DATAGRAMAS EN " + MACHINE + " Puerto: " + PORT);
            InetSocketAddress endPoint = new InetSocketAddress(MACHINE,PORT);
            DatagramSocket datagramSocket = new DatagramSocket(endPoint);

            System.out.println("ESPERANDO UN MENSAJE...");
            byte[] message = new byte[40];

            DatagramPacket datagramPacket = new DatagramPacket(message,message.length);

            while(true){

                datagramSocket.receive(datagramPacket); // Se bloquea hasta que se recibe un mensaje

                System.out.println(new String(message,0,datagramPacket.getLength()));

            }

        } catch (SocketException e){
            System.out.println("nERROR EN EL SOCKET: " + e.getLocalizedMessage());
        } catch (IOException e){
            System.err.println("nI/O ERROR: " + e.getLocalizedMessage());
        }


    }

}
