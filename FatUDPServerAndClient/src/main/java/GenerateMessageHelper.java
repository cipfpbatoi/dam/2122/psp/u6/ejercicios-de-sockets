public class GenerateMessageHelper {

    public static String GenerarSecuencia(char caracter, int numeroRepeticiones)
    {
        StringBuilder eBuilder = new StringBuilder();

        for (int i = 0; i<numeroRepeticiones;i++)
        {
            eBuilder.append(caracter);
        }

        return eBuilder.toString();
    }

}
