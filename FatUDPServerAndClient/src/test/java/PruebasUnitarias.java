import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasUnitarias {


    @Test
    @DisplayName("Comprobamos cual es el tamaño en UTF8 de la cadena y si concide")
    public void AssertSize()
    {
        int lenghtString = 80000;

        String E = GenerateMessageHelper.GenerarSecuencia('E',lenghtString);
        assertEquals(lenghtString,E.length(),"Error en el bucle for de Inicialización");
        byte[] ebytes = E.getBytes(StandardCharsets.UTF_8);
        assertEquals(lenghtString,ebytes.length,"Debe revisarse la codificación o el mensaje porque cada carcter" +
                "se codifica en más de un byte");

    }

}
