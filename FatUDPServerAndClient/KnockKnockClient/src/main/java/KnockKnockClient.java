import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class KnockKnockClient {



    public static void main(String[] args) {

        //Valores por defecto
        String hostName = "localhost";
        int portNumber = 4444;

        if ( args.length >= 1 ){
            hostName = args[0];
        }

        if ( args.length >= 2 ){
            portNumber = Integer.parseInt(args[1]);
        }

        //Inicializamos la conexión
        InetSocketAddress endPoint = new InetSocketAddress(hostName,portNumber);

        try (
                Socket clientSocket = new Socket();

                ) {

            clientSocket.connect(endPoint);
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

            String fromServer = "", fromUser = "";
            Scanner stdIn = new Scanner(System.in);

            while ((fromServer = in.readLine()) != null) {
                System.out.println("Server: " + fromServer);
                if (fromServer.equals("Bye."))
                    break;

                fromUser = stdIn.nextLine();
                if (fromUser != null) {
                    System.out.println("Client: " + fromUser);
                    out.println(fromUser);
                }
            }


        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }


    }
}
