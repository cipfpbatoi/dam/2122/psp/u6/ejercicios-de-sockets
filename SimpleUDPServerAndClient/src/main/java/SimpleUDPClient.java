import java.io.IOException;
import java.net.*;

public class SimpleUDPClient {

    private static final int TO_PORT = 1234;
    private static final String TO_MACHINE = "localhost";

    public static void main(String[] args) {

        try {


            System.out.println("CREANDO EL SOCKET DE DATAGRAMAS");
            // Enlaza el socket con cualquier puerto disponible elegido  por el kernel
            DatagramSocket datagramSocket = new DatagramSocket();

            System.out.println(".... ENVIANDO EL MENSAJE ...");
            String message = ">>>> Hola soy un CLIENT UDP <<<<";
            DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(),message.getBytes().length,
            InetAddress.getByName(TO_MACHINE),TO_PORT);

            datagramSocket.send(datagramPacket);
            System.out.println(".... MENSAJE ENVIADO.");

            System.out.println(".... ESPERANDO RESPUESTA ....");
            byte[] response = new byte[40];
            datagramPacket = new DatagramPacket(response,response.length);
            datagramSocket.receive(datagramPacket);

            System.out.println(".... MENSAJE RECIBIDO: " + new String(response));

            System.out.println("CERRANDO EL SOCKET");
            datagramSocket.close();
            System.out.println("CLIENTE FINALIZADO");

        } catch (SocketException e){
            System.out.println("nERROR EN EL SOCKET: " + e.getLocalizedMessage());
        } catch (IOException e){
            System.err.println("nI/O ERROR: " + e.getLocalizedMessage());
        }

    }
}
