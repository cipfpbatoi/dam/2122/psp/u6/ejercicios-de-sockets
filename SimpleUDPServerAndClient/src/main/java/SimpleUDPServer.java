import java.io.IOException;
import java.net.*;

public class SimpleUDPServer {

    private static final int PORT = 1234;
    private static final String MACHINE = "localhost";

    public static void main(String[] args) {

        try {
            System.out.println("CREANDO SOCKET DE DATAGRAMAS EN " + MACHINE + " Puerto: " + PORT);
            InetSocketAddress endPoint = new InetSocketAddress(MACHINE,PORT);
            DatagramSocket datagramSocket = new DatagramSocket(endPoint);

            System.out.println("ESPERANDO UN MENSAJE...");
            byte[] message = new byte[40];

            DatagramPacket datagramPacket = new DatagramPacket(message,message.length);
            datagramSocket.receive(datagramPacket); // Se bloquea hasta que se recibe un mensaje

            //Mensaje recibido: podemos obtener información de la máquina remota que ha enviado el datagrama
            InetAddress senderAddress = datagramPacket.getAddress();
            int senderPort = datagramPacket.getPort();

            System.out.println(".... MENSAJE DE ["
                    +   senderAddress.getHostAddress() + ","
                    +   senderPort + "] RECIBIDO: "
                    +   new String(message));

            System.out.println(".... ENVIANDO RESPUESTA AL EMISOR...");
            String s = ">>>>Mensaje Recibido Correctamente<<<<";
            byte[] messageBack = s.getBytes();

            DatagramPacket datagramPacketBack = new DatagramPacket(messageBack, messageBack.length,senderAddress,senderPort);
            datagramSocket.send(datagramPacketBack);

            System.out.println("RESPUESTA ENVIADA, CERRANDO EL SOCKET");
            datagramSocket.close();
            System.out.println("SERVIDOR FINALIZADO");

        } catch (SocketException e){
            System.out.println("nERROR EN EL SOCKET: " + e.getLocalizedMessage());
        } catch (IOException e){
            System.err.println("nI/O ERROR: " + e.getLocalizedMessage());
        }


    }

}
