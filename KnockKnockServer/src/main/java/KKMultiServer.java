import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class KKMultiServer {

    public static void main(String[] args) {

        int portNumber = 4444;

        ExecutorService executors = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        if (args.length == 1){
            try {
                portNumber = Integer.parseInt(args[0]);
            } catch (Exception e){
                System.err.println(e.getLocalizedMessage());
            }
        }

        try (
                ServerSocket serverSocket = new ServerSocket(portNumber);

        ){
            while (true){
                Socket clientSocket = serverSocket.accept();
                executors.execute(new KKMultiServerThread(clientSocket));
            }

        } catch (IOException e){
            System.out.println(e.getLocalizedMessage());
            System.err.println("Finalizando el Thread Pool");
            executors.shutdown();

            try {
                if (!executors.awaitTermination(10, TimeUnit.SECONDS)){
                    executors.shutdownNow();
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

    }

}
