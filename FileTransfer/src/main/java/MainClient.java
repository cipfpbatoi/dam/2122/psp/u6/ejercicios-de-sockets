import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class MainClient {

    public static final int DEFAULT_REMOTE_PORT = 4444;
    public static final String DEFAULT_REMOTE_HOST = "localhost";
    public static final String DEFAULT_FILE_TO_SEND = "prueba.txt";

    public static void main(String[] args) {

        String file = args.length >= 1 ? args[0] : DEFAULT_FILE_TO_SEND;
        String remote_host = args.length >= 2 ? args[1] : DEFAULT_REMOTE_HOST;
        int remote_port = args.length >= 3 ? Integer.parseInt(args[2]) : DEFAULT_REMOTE_PORT;

        File f = new File(file);

        if (!f.exists()){
            System.err.println("The file " + f.getAbsoluteFile() + " doesn't exist");
            System.exit(1);
        }

        InetSocketAddress inetSocketAddress = new InetSocketAddress(remote_host,remote_port);

        try (
            Socket socket = new Socket();

        ){
            socket.connect(inetSocketAddress);

            try (FileSender fileSender = new FileSender(f,socket);)
            {
                fileSender.send();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
