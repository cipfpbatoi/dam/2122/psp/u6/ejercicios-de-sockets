import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class FileServerReceiver {

    public static final int DEFAULT_PORT = 4444;
    public static final String DEFAULT_HOST = "localhost";

    public static void main(String[] args) {

        InetSocketAddress inetSocketAddress = new InetSocketAddress(DEFAULT_HOST,DEFAULT_PORT);

        try (ServerSocket serverSocket = new ServerSocket(inetSocketAddress.getPort())){

            Socket clientSocket = serverSocket.accept();

            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            System.out.println("FILENAME: " + br.readLine());

            br.lines().forEach(System.out::println);

            br.close();
            clientSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
