import java.io.*;
import java.net.Socket;

public class FileSender implements AutoCloseable{
    File f;
    Socket serverSocket;
    BufferedReader br;
    BufferedWriter bw;
    BufferedReader fbr;
    public FileSender(File file, Socket serverSocket) {
        this.f = file;
        this.serverSocket = serverSocket;
    }


    public void send() throws IOException {
        br = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
        bw = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()));
        fbr = new BufferedReader(new InputStreamReader(new FileInputStream(f)));

        bw.write(f.getName());
        bw.newLine();
        bw.flush();


        fbr.lines().forEach( (l) -> {
            try {
                bw.write(l);
                bw.newLine();
                bw.flush();
            } catch (IOException e){
                System.err.println(e.getLocalizedMessage());
            }
        });


    }

    @Override
    public void close() throws Exception {
        if (br != null){
            br.close();
        }
        if (bw != null){
            bw.close();
        }
        if (fbr != null){
            fbr.close();
        }
    }
}
