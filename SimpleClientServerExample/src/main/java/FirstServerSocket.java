import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class FirstServerSocket {

    private static final int PORT = 4444;
    private static final String MACHINE = "0.0.0.0";

    // sudo lsof -i -P | grep LISTEN | grep :4444


    public static void main(String[] args)  {

        try {

            System.out.println("CREATING SERVER SOCKET");

            // 1. Primero Creamos el ServerSocket
            ServerSocket serverSocket = new ServerSocket();
            System.out.printf("BINDING TO SOCKET [%s,%d]",MACHINE,PORT);
            System.out.println();

            //2. Creamos el objeto InetSocketAddress que se utiliza para
            //mapear el endopoint donde vamos a escuchar ( o conectar en el caso de que fuera un cliente )
            InetSocketAddress sockAddr = new InetSocketAddress(MACHINE,PORT);
            //3. Realizamos el Bind del Servicio, si falla, es porque ya había
            // algun servicio escuchando o por otros motivos.
            serverSocket.bind(sockAddr);

            System.out.println("ACCEPTING CLIENT CONNECTIONS...");
            // 4. Esperamos a que un cliente se conecte, cuando se
            // conecte, nos devolverá un objeto de tipo socket.
            // esta llamada, bloquea la conexión
            Socket clientSocket = serverSocket.accept();
            System.out.println("....... CLIENT CONNECTION STABLISHED ON PORT "+ clientSocket.getPort());

            // I/O Stream Creation
            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            // Read Message from the client
            String message = br.readLine();
            System.out.println("..... MESSAGE FROM CLIENT RECEIVED: "+message);

            // Send message to the client:
            message = ">>>> Thank you. Message Recived Ok! <<<<";
            bw.write(message);
            bw.newLine();
            bw.flush();

            //Closing client and server connection
            br.close();
            bw.close();
            clientSocket.close();
            serverSocket.close();

            System.out.println("SERVER FINISHED: CONNECTION CLOSED");

        } catch (IOException e){
            System.err.println(e.getLocalizedMessage());
        }





    }

}
