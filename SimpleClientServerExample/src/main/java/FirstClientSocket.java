import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class FirstClientSocket {

    private static final int TO_PORT = 4444;
    private static final String TO_MACHINE = "localhost";

    public static void main(String[] args) {
        try {
            System.out.println("CREATING CLIENT SOCKET");
            // 1. Creamos el socket por el que nos comunicaremos.
            Socket clientSocket = new Socket();

            System.out.printf("TRYING TO CONNECT TO THE SERVER [%s,%d]", TO_MACHINE, TO_PORT);
            System.out.println();
            // 2. Creamos el InetSocketAddress para indicar donde vamos a conectarnos
            InetSocketAddress sockAddr = new InetSocketAddress(TO_MACHINE, TO_PORT);
            // 3. Intentamos conectarnos con el servidor
            clientSocket.connect(sockAddr);

            System.out.println("CONNECTION STABLISHED ON PORT "+ clientSocket.getPort());

            // IO Stream Creation
            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            System.out.println(".... SENDING MESSAGE ....");
            bw.write(">>> Hello, I'm the client! <<<<");
            bw.newLine();
            bw.flush();

            System.out.println(".... WAITING FOR SERVER RESPONSE ....");
            String message = br.readLine();

            System.out.println("............ RESPONSE FROM SERVER: " + message);

            //Closing connection
            br.close();
            bw.close();
            clientSocket.close();
            System.out.println("CLIENT FINISHED: CONNECTION CLOSED");



        } catch (IOException e){
            System.err.println(e.getLocalizedMessage());
        }
    }

}
