import java.io.IOException;
import java.net.*;

public class MulticastReceiver {

    private static final int PORT_TO_LISTEN = 4444;
    private static final String FROM_MULTICAST_LISTEN = "230.0.0.1";

    public static void main(String[] args) {
        //-Djava.net.preferIPv4Stack=true
        InetSocketAddress inetSocketAddress = new InetSocketAddress(FROM_MULTICAST_LISTEN,PORT_TO_LISTEN);



        try (MulticastSocket s = new MulticastSocket(inetSocketAddress.getPort())){

            //s.joinGroup(inetSocketAddress.getAddress());
            s.joinGroup(inetSocketAddress,NetworkInterface.getByIndex(0));

            byte[] dato = new byte[1000];

            DatagramPacket dgp = new DatagramPacket(dato, dato.length);

            while (true) {
                s.receive(dgp);
                System.out.println("Recived from UDP: " + new String(dgp.getData(), 0, dgp.getLength()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
