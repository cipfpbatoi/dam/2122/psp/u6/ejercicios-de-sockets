import java.io.IOException;
import java.net.*;

public class MultiCastSender {

    private static final int TO_PORT = 4444;
    private static final String TO_MULTICAST = "230.0.0.1";

    public static void main(String[] args) {

        String msg = "Hello";
        InetSocketAddress inetSocketAddress = new InetSocketAddress(TO_MULTICAST,TO_PORT);

        try (DatagramSocket s = new DatagramSocket())
        {

            DatagramPacket hi = new DatagramPacket(msg.getBytes(), msg.length(),
                    inetSocketAddress.getAddress(), inetSocketAddress.getPort());

            s.send(hi);

        } catch (IOException e){
            System.err.println(e.getLocalizedMessage());
        }

    }

}
